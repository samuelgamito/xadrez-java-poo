package chessgame;
import org.junit.jupiter.api.Test;

import chessgame.Controller.GameController;
import chessgame.Model.*;
import chessgame.Threads.*;
import chessgame.View.*;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 * Essa é a classe principal do Jogo de Xadrez 
 */
public class ChessGame{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Criando e inicializando as variaveis do MVC */
        BoardPanel tabuleiro      = new BoardPanel();
        GameFrame view            = new GameFrame(tabuleiro);
        ChessModel model          = new ChessModel();
        GameController controller = new GameController();
        FilesModel fModel         = new FilesModel();
        ChessInfos gInfoView      = new ChessInfos(model);
        
        /**Inserindo as variveis nas classes para que elas possam acessar 
        umas às outras**/
        
        tabuleiro.addObserver(view);
        
        controller.setView(view,gInfoView);
        controller.setModel(model, fModel);

        view.setController(controller);
        view.setModel(model);
        
        //Criando e estartando Thread do tabuleiro
        TabuleiroThread tab     = new TabuleiroThread(view, model, controller);
        NewGame novo            = new NewGame(view,model, controller);
        ChessInfosThread infos  = new ChessInfosThread(gInfoView, model, controller);
        AutoSaveThread autoSave = new AutoSaveThread();
        autoSave.setModel(model);
        
        
        
        model.setTabuleiroThread(tab);
        model.setInitGameThread(novo);
        model.setAutoSaveThread(autoSave);
        model.setGameInfosThread(infos);
        novo.start();
    }
    
    
}
