/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Exceptions;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 * 
 */
@SuppressWarnings("serial")
public class LoadFileException extends Exception {

    /**
     * Creates a new instance of <code>LoadFileException</code> without detail
     * message.
     */
    public LoadFileException() {
    }

    /**
     * Constructs an instance of <code>LoadFileException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public LoadFileException(String msg) {
        super(msg);
    }
}
