package chessgame;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.BeforeClass;
import chessgame.Controller.GameController;
import chessgame.Exceptions.LoadFileException;
import chessgame.Model.ChessModel;
import chessgame.Model.FilesModel;
import chessgame.Pecas.Peca;
import chessgame.Threads.AutoSaveThread;
import chessgame.Threads.ChessInfosThread;
import chessgame.Threads.NewGame;
import chessgame.Threads.TabuleiroThread;
import chessgame.View.BoardPanel;
import chessgame.View.ChessInfos;
import chessgame.View.GameFrame;
import junit.framework.Assert;

class Funcional {
	private static final BoardPanel tabuleiro      = new BoardPanel();
	private static final GameFrame view            = new GameFrame(tabuleiro);
	private static ChessModel model          = new ChessModel();
	private static final GameController controller = new GameController();
	private static final FilesModel fModel         = new FilesModel();
	private static final ChessInfos gInfoView      = new ChessInfos(model);
	
	@BeforeAll
	public static void setUp() throws Exception {
		tabuleiro.addObserver(view);
        
        controller.setView(view,gInfoView);
        controller.setModel(model, fModel);

        view.setController(controller);
        view.setModel(model);
        
        //Criando e estartando Thread do tabuleiro
        TabuleiroThread tab     = new TabuleiroThread(view, model, controller);
        NewGame novo            = new NewGame(view,model, controller);
        ChessInfosThread infos  = new ChessInfosThread(gInfoView, model, controller);
        AutoSaveThread autoSave = new AutoSaveThread();
        autoSave.setModel(model);
        
        
        
        model.setTabuleiroThread(tab);
        model.setInitGameThread(novo);
        model.setAutoSaveThread(autoSave);
        model.setGameInfosThread(infos);
        novo.start();
		
	}

	
	@Test
	/**
	 * Primeiro movimento deve ser das peças brancas e testando se esta selecionando a cor do jogador atual
	 */
	public void firstMove() {
		controller.moveByCommand(0, 1);
		assertNull(model.getSelectedPeca());
		controller.moveByCommand(7, 7);
		assertEquals(model.getSelectedPeca().getCor().getName(), model.getJogadorAtual().getName());
	}

	@Test
	/**
	 *  Verifica se apos um movimento ocorre a troca de cor 
	 */
	public void colorChange() throws Exception {

		String oldColor = model.getJogadorAtual().getName() ;//Salvando cor do jogador inicial
		controller.moveByCommand(6, 6);//selecionando um peao branco
		controller.moveByCommand(6, 5); //movendo um peao branco
		 
		try {
			assertNotEquals(oldColor, model.getJogadorAtual().getName());
		} catch (AssertionError e) {
			System.err.println("Cor não alterada.");;
		}
			

	}
	private List<String> readCenarios(String filename)
	{
	  List<String> records = new ArrayList<String>();
	  try
	  {
	    BufferedReader reader = new BufferedReader(new FileReader(filename));
	    String line;
	    while ((line = reader.readLine()) != null)
	    {
	      records.add(line);
	    }
	    reader.close();
	    return records;
	  }
	  catch (Exception e)
	  {
	    System.err.format("Exception occurred trying to read '%s'.", filename);
	    e.printStackTrace();
	    return null;
	  }
	}
	
	@Test
	/**
	 * Testando os movientos das peças, os arquivos de inicialização e teste 
	 * se encontram na pasta save/test
	 * nome dos arquivos de inicialização : peca_tipo-de-test.txt
	 * nome dos arquivos de teste         : peca_tipo-de-test_t.txt
	 */
	public void testMovimentosFuncional() throws Exception {
		int total = 0;
		System.out.println("teste de movimentos");
		int total_de_testes, i,j,m,n,xi, yi, xf, yf;
		int tabuleiro_oraculo[][] = new int[8][8];
		
		List<String> cenarios = readCenarios("/home/samuelgamito/eclipse-workspace/ChessGame/saves/funcional/cenarios.txt");
		
		for(String cenario : cenarios) {
			
			//Carregando casos de teste
			try {
				String dir = new File(".").getCanonicalPath()
				        +"/saves/funcional/"+cenario+"_t.txt";
				try (BufferedReader br = 
	                    new BufferedReader(new FileReader(dir))){
					
					Scanner line;
		            //Carregando primeira linha para a classe scanner 
		            //e assim percorrer cada campo de dado do arquivo
		            
		            line = new Scanner(br);
		            total_de_testes = line.nextInt();
		            total += total_de_testes;
		            System.out.println(cenario);
		            for(i = 0; i<total_de_testes; ++i) {
		            	
		            	for(m=0; m<8; ++m) {
		            		for (n = 0 ; n<8; ++n) {
			            		tabuleiro_oraculo[m][n] = 0;
		            		}
		            	}
		            	model = new ChessModel();
		            	controller.setModel(model, fModel);
		    			TabuleiroThread tab     = new TabuleiroThread(view, model, controller);
		    	        NewGame novo            = new NewGame(view,model, controller);
		    	        ChessInfosThread infos  = new ChessInfosThread(gInfoView, model, controller);
		    	        AutoSaveThread autoSave = new AutoSaveThread();
		    	        autoSave.setModel(model);
		    			model.setTabuleiroThread(tab);
		    	        model.setInitGameThread(novo);
		    	        model.setAutoSaveThread(autoSave);
		    	        model.setGameInfosThread(infos);
		    	        novo.start();
		    	        
		    			try {
		    				fModel.loadSave(model, "funcional/"+cenario+".txt");
		    			} catch (LoadFileException e) {
		    				// TODO Auto-generated catch block
		    				e.printStackTrace();
		    			}
		    			
		            	xi = line.nextInt();
		            	yi = line.nextInt();
		            	xf = line.nextInt();
		            	yf = line.nextInt();
		            	//Carreando oráculo
		            	for(j=0; j<32; j++) {
		            		int x =line.nextInt(), y = line.nextInt();
		            		tabuleiro_oraculo[y][x] = line.nextInt();
		            	}
		            	
		            	
		            	controller.moveByCommand(xi, yi);
		        		controller.moveByCommand(xf, yf);
		        		try {
		        			for(m=0; m<8; ++m) {
			            		assertArrayEquals(tabuleiro_oraculo[m], model.getTabuleiro()[m]);
			            	}
		        		} catch (AssertionError e) {

		        			System.out.println("entregue");
		        			for(m=0; m<8; ++m) {
			            		for (n = 0 ; n<8; ++n) {
				            		System.out.print(model.getTabuleiro()[m][n]+"\t");
			            		}
			            		System.out.println();
			            	}

		        			System.out.println("esperado");
		        			for(m=0; m<8; ++m) {
			            		for (n = 0 ; n<8; ++n) {
				            		System.out.print(tabuleiro_oraculo[m][n]+"\t");
			            		}
			            		System.out.println();
			            	}
		        			System.err.println("Fail:"+cenario+" teste:"+i);;
		        		}
		            }
				}
				System.out.println("TOTAL DE TESTES: "+total);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//Adicionando testes estrutural
	
	@Test
	/**
	 * Selecionando uma posição nula ou vazia
	 */
	
	public void posicaoNula () {
		model = new ChessModel();
    	controller.setModel(model, fModel);
		TabuleiroThread tab     = new TabuleiroThread(view, model, controller);
        NewGame novo            = new NewGame(view,model, controller);
        ChessInfosThread infos  = new ChessInfosThread(gInfoView, model, controller);
        AutoSaveThread autoSave = new AutoSaveThread();
        autoSave.setModel(model);
		model.setTabuleiroThread(tab);
        model.setInitGameThread(novo);
        model.setAutoSaveThread(autoSave);
        model.setGameInfosThread(infos);
        novo.start();
        
        controller.moveByCommand(4, 4);
        try {
            assertNull(model.getSelectedPeca());
		} catch (AssertionError e) {
			System.err.println("Fail:Peça foi selecionada");
		}
		
	}
	
	@Test
	public void testMovimentosEstrutural() throws Exception {
		int total = 0;
		System.out.println("teste de movimentos");
		int total_de_testes, i,j,m,n,xi, yi, xf, yf;
		int tabuleiro_oraculo[][] = new int[8][8];
		
		List<String> cenarios = readCenarios("/home/samuelgamito/eclipse-workspace/ChessGame/saves/estrutural/cenarios.txt");
		
		for(String cenario : cenarios) {
			//Carregando casos de teste
			try {
				String dir = new File(".").getCanonicalPath()
				        +"/saves/estrutural/"+cenario+"_t.txt";
				try (BufferedReader br = 
	                    new BufferedReader(new FileReader(dir))){
					
					Scanner line;
		            //Carregando primeira linha para a classe scanner 
		            //e assim percorrer cada campo de dado do arquivo
		            
		            line = new Scanner(br);
		            total_de_testes = line.nextInt();
		            total += total_de_testes;
		            System.out.println(cenario);
		            for(i = 0; i<total_de_testes; ++i) {
		            	
		            	for(m=0; m<8; ++m) {
		            		for (n = 0 ; n<8; ++n) {
			            		tabuleiro_oraculo[m][n] = 0;
		            		}
		            	}
		            	model = new ChessModel();
		            	controller.setModel(model, fModel);
		    			TabuleiroThread tab     = new TabuleiroThread(view, model, controller);
		    	        NewGame novo            = new NewGame(view,model, controller);
		    	        ChessInfosThread infos  = new ChessInfosThread(gInfoView, model, controller);
		    	        AutoSaveThread autoSave = new AutoSaveThread();
		    	        autoSave.setModel(model);
		    			model.setTabuleiroThread(tab);
		    	        model.setInitGameThread(novo);
		    	        model.setAutoSaveThread(autoSave);
		    	        model.setGameInfosThread(infos);
		    	        novo.start();
		    	        
		    			try {
		    				fModel.loadSave(model, "funcional/"+cenario+".txt");
		    			} catch (LoadFileException e) {
		    				// TODO Auto-generated catch block
		    				e.printStackTrace();
		    			}
		    			
		            	xi = line.nextInt();
		            	yi = line.nextInt();
		            	xf = line.nextInt();
		            	yf = line.nextInt();
		            	//Carreando oráculo
		            	for(j=0; j<32; j++) {
		            		int x =line.nextInt(), y = line.nextInt();
		            		tabuleiro_oraculo[y][x] = line.nextInt();
		            	}
		            	
		            	
		            	controller.moveByCommand(xi, yi);
		        		controller.moveByCommand(xf, yf);
		        		try {
		        			for(m=0; m<8; ++m) {
			            		assertArrayEquals(tabuleiro_oraculo[m], model.getTabuleiro()[m]);
			            	}
		        		} catch (AssertionError e) {

		        			System.out.println("entregue");
		        			for(m=0; m<8; ++m) {
			            		for (n = 0 ; n<8; ++n) {
				            		System.out.print(model.getTabuleiro()[m][n]+"\t");
			            		}
			            		System.out.println();
			            	}

		        			System.out.println("esperado");
		        			for(m=0; m<8; ++m) {
			            		for (n = 0 ; n<8; ++n) {
				            		System.out.print(tabuleiro_oraculo[m][n]+"\t");
			            		}
			            		System.out.println();
			            	}
		        			System.err.println("Fail:"+cenario+" teste:"+i);;
		        		}
		            }
				}
				System.out.println("TOTAL DE TESTES: "+total);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

}
