package chessgame.View;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observer;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
*
* @author Samuel de Olivera Gamito
* @version 0.9
*/
public class BoardPanel extends JPanel{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//<editor-fold defaultstate="collapsed" desc="Variaveis do Sistema">
    private final ArrayList<Observer> observers;
    protected final static String greenPath = "image/green.png";
    protected static BufferedImage greenImg = null;
    
    protected final static String whitePath = "image/white.png";
    BufferedImage whiteImg = null;
    
    protected final static String selectedPath = "image/selected.png";
    protected static BufferedImage selectedImg = null;
    
    protected final static String hoverPath = "image/hover.png";
    protected static BufferedImage hoverImg = null;
//</editor-fold>

    
    //<editor-fold defaultstate="collapsed" desc="Metodos construtors da Classe">
    public BoardPanel(){
        observers = new ArrayList<>();
        loadImages();
    }
    public BoardPanel(Observer view, Observer model){
        observers = new ArrayList<>();
        //Carregando imagem
        loadImages();
        
        //Adicionando os observers
        observers.add(view);
        observers.add(model);
    }
//</editor-fold>
    
    private void loadImages(){
        //Carregando as imagens
        try {
            greenImg     = ImageIO.read(new File(greenPath));
            whiteImg     = ImageIO.read(new File(whitePath));
            selectedImg  = ImageIO.read(new File(selectedPath));
            hoverImg     = ImageIO.read(new File(hoverPath));
        } catch (IOException ex) {
            System.err.println(ex.toString());
        }
    }
    
    
    
    //<editor-fold defaultstate="collapsed" desc="Funções para desenhar no tabuleiro">
    public void drawBoard(Graphics2D g){
        int i,j;
        boolean color = true;
        //Pegando as dimensões do tabuleiros
        final int dx = 61, dy=61;
        BufferedImage selectedImage = null;
        
        //Looping para imprimir o tabuleiro no painel
        for(i = 0; i<8; ++i){
            for( j = 0; j<8; ++j){
                //varia a cor do quadrante
                if(color){
                    selectedImage = greenImg;
                }else{
                    selectedImage = whiteImg;
                }
                
                g.drawImage(selectedImage, i*dx, j*dy , i*dx+dx, j*dy + dy, i*dx, j*dy , i*dx+dx, j*dy + dy, null);
                color = !color;
            }
            
            color = !color;
        }
        
    }
    
    public void drawMouseHover(Graphics2D g, int x, int y){
        g.drawImage(
                hoverImg,
                x*61,y*61,
                61, 61, null);
    }
    void drawSelectedBorder(Graphics2D g, Point quadrante) {
        g.drawImage(
                selectedImg,
                quadrante.x*61,quadrante.y*61,
                61, 61, null);
    }
    

    void drawPath(Graphics2D g, ArrayList<Point> moves) {
        for(Point p :moves){
            g.drawImage(
                    selectedImg,
                    p.x*61,p.y*61,
                    61, 61, null);
        }
    }
//</editor-fold>
    
    public void addObserver(Observer o){
        observers.add(o);
    }
    
    
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        
        Graphics2D graficos = (Graphics2D)g;
        drawBoard(graficos);
        
        for(Observer ob : observers){
            ob.update(null, g);
        }
    }

    

}
