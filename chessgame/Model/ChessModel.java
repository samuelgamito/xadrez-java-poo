/*
 */
package chessgame.Model;

import chessgame.Pecas.*;
import chessgame.Pecas.Peca.Cor;
import chessgame.Threads.*;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.*;
import java.util.logging.*;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class ChessModel {
    private final ArrayList<Peca> pecasPretas;  // uma lista das pecas pretas
    private final ArrayList<Peca> pecasBrancas; // uma lista das pecas brancas
    private final TimerCounter counterBranco;
    private final TimerCounter counterPreto;
    private final TimerCounter counterTotal;
    private final int tabuleiro[][] = new int[8][8];
    private int mouseX, mouseY;
    private int selectedX, selectedY;
    private TabuleiroThread tabuleiroThread;
    private AutoSaveThread autoSaveThread;
    private ChessInfosThread infos;
    private NewGame initGameThread;
    private Peca selectedPeca;
    private Cor jogadorAtual;
    private String nomePreto, nomeBranco, fileName, consoleMessage;
    private int saveInterval;
    private final boolean counteLoopSit = true;
    
    
    public ChessModel()  {
        this.selectedPeca  = null;
        this.pecasPretas   = new ArrayList<Peca>();
        this.pecasBrancas  = new ArrayList<Peca>();
        this.jogadorAtual  = Cor.BRANCO;
        
        this.counterBranco = new TimerCounter();
        this.counterPreto  = new TimerCounter();
        this.counterTotal  = new TimerCounter();
        this.consoleMessage     = "";
        init();
    }
    private void init() {        
        int i;
        System.out.println("Criando time");
        //inicializa time branco
         pecasBrancas.add(new Rei(Peca.Cor.BRANCO,4,7));
        tabuleiro[7][4] = 1;
        pecasBrancas.add(new Rainha(Peca.Cor.BRANCO,3,7));
        tabuleiro[7][3] = 2;
        pecasBrancas.add(new Bispo(Peca.Cor.BRANCO,5,7));
        tabuleiro[7][5] = 3;
        pecasBrancas.add(new Bispo(Peca.Cor.BRANCO,2,7));
        tabuleiro[7][2] = 3;
        pecasBrancas.add(new Cavalo(Peca.Cor.BRANCO,1,7));
        tabuleiro[7][1] = 5;
        pecasBrancas.add(new Cavalo(Peca.Cor.BRANCO,6,7));
        tabuleiro[7][6] = 5;
        pecasBrancas.add(new Torre(Peca.Cor.BRANCO,0,7));
        tabuleiro[7][0] = 4;
        pecasBrancas.add(new Torre(Peca.Cor.BRANCO,7,7));
        tabuleiro[7][7] = 4;
        for(i=0;i<8; ++i){
            pecasBrancas.add(new Peao(Peca.Cor.BRANCO,i,6));
            tabuleiro[6][i] = 6;
            
            pecasBrancas.add(new Peao(Peca.Cor.PRETO,i,1));
            tabuleiro[1][i] = -6;
        }
        
        //inicializa time preto
        pecasBrancas.add(new Rei(Peca.Cor.PRETO,4,0));
        tabuleiro[0][4] = -1;
        pecasBrancas.add(new Rainha(Peca.Cor.PRETO,3,0));
        tabuleiro[0][3] = -2;
        pecasBrancas.add(new Bispo(Peca.Cor.PRETO,2,0));
        tabuleiro[0][2] = -3;
        pecasBrancas.add(new Bispo(Peca.Cor.PRETO,5,0));
        tabuleiro[0][5] = -3;
        pecasBrancas.add(new Cavalo(Peca.Cor.PRETO,1,0));
        tabuleiro[0][1] = -5;
        pecasBrancas.add(new Cavalo(Peca.Cor.PRETO,6,0));
        tabuleiro[0][6] = -5;
        pecasBrancas.add(new Torre(Peca.Cor.PRETO,7,0));
        tabuleiro[0][7] = -4;
        pecasBrancas.add(new Torre(Peca.Cor.PRETO,0,0));
        tabuleiro[0][0] = -4;
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public void setMouseCoord(int mouseX, int mouseY){
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    }
    public void setSelectedCoord(int x, int y){
        this.selectedX = x;
        this.selectedY = y;
        
    }
    public int getMouseX(){
        return this.mouseX;
    }
    public int getMouseY(){
        return this.mouseY;
    }
    public Cor getJogadorAtual(){
        return this.jogadorAtual;
    }
    public String getJogadorTxt(){
       if(jogadorAtual.getValor() == 0) 
           return this.nomeBranco+" - Peça "+Cor.BRANCO.getName();
       else 
           return this.nomePreto+" - Peça "+Cor.PRETO.getName();
    }
    public void setGameSettings(long saveName, String jPreto, String jBranco, int tSave) {
        this.nomeBranco   = jBranco;
        this.nomePreto    = jPreto;
        this.saveInterval = tSave;
        this.fileName     = Long.toString(saveName)+"-"+jBranco+"-"+jPreto+".txt";
    }
    public String getGameSettings(){
        //nBranco nPreto Atual timeSave fileName
        return nomeBranco+" "+nomePreto+" "+jogadorAtual+" "+saveInterval+" "+fileName;
    }
    public String getFileName(){
        return this.fileName;
    }
    public void setTabuleiroThread(TabuleiroThread tab) {
        this.tabuleiroThread = tab;
    }
    public void setInitGameThread(NewGame runn){
        this.initGameThread = runn;
    }
    public void setAutoSaveThread(AutoSaveThread runn){
        this.autoSaveThread = runn;
    }
    public Peca getSelectedPeca() {
        return this.selectedPeca;
    }
    public void setSelectedPeca(Peca s){
        this.selectedPeca = s;
    }
    public void setQuadranteSelected(int x, int y) {
        int tempPecaValue = tabuleiro[selectedPeca.getQuadrante().y][selectedPeca.getQuadrante().x];
        tabuleiro[selectedPeca.getQuadrante().y][selectedPeca.getQuadrante().x] = 0;
        tabuleiro[y][x] = tempPecaValue;
        this.selectedPeca.setQuadrante(x, y);
        this.selectedPeca.setFirstMove(false);
        
    }
    public String getPecasBrancasSave(){
        String retorno = "";
        //tipo cor x y firstMove visible
        for(Peca p: pecasBrancas){
            retorno += p.getInfosSave();
            retorno += "\n";
        }
        
        return retorno;
    }
    public String getPecasPretasSave(){
        String retorno = "";
        //tipo cor x y firstMove visible
        for(Peca p: pecasPretas){
            retorno += p.getInfosSave();
            retorno += "\n";
        }
        
        return retorno;
    }
    

    public void setLoadGameSettings(String nBranco, String nPreto, 
            String atual, int saveTime, String fileName) {
        
        this.nomeBranco   = nBranco;
        this.nomePreto    = nPreto;
        this.jogadorAtual = Cor.valueOf(atual);
        this.saveInterval = saveTime;
        this.fileName     = fileName;
    }
    public void setLoadPeca(int xi, int yi, int x, int y, 
            int firstMove, int visible) {
        boolean fMove = (firstMove ==1), v =(visible == 1);
        
        Peca p = this.searchPeca(xi, yi);

        int tempPecaValue = tabuleiro[yi][xi];
        tabuleiro[yi][xi] = 0;
        tabuleiro[y][x] = tempPecaValue;
        
        p.setVisible(v);
        p.setQuadrante(x, y);
        p.setFirstMove(fMove);
    }
    

    public void setGameInfosThread(ChessInfosThread infos) {
        this.infos = infos;
    }
    
    public boolean getCounteLoopSit() {
        return this.counteLoopSit;
    }
    
    public String getCounterBrancoTxt(){
        return this.counterBranco.toString();
    }
    public String getCounterPretasTxt(){
        return this.counterPreto.toString();
    }
    
    public String getCounterTotalTxt(){
        return this.counterTotal.toString();
    }
    public String getCounters(){
        return counterTotal.toSave()+"\n"+
            counterBranco.toSave()+"\n"+
            counterPreto.toSave();
    }
    public void setCounterTotal(int m, int s) {
          counterTotal.setCounter(m,s);
    }
    public void setCounterBranco(int m, int s) {
          counterBranco.setCounter(m,s);
    }
    public void setCounterPreto(int m, int s) {
          counterPreto.setCounter(m,s);
    }
    public String getConsoleMessage(){
        return consoleMessage;
    }
    
    public int[][] getTabuleiro(){
    	return this.tabuleiro;
    }
    
    public void generateMoves() {
    	pecasBrancas.stream().map((p) -> {
            return p;
        }).forEach((p) -> {
        	p.generateMoves(tabuleiro, pecasBrancas, pecasPretas);
        });
        
        //desenha pecas pretas
        pecasPretas.stream().map((p) -> {
            return p;
        }).forEach((p) -> {
            p.generateMoves(tabuleiro, pecasBrancas, pecasPretas);
        });
    }
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Comandos do tabuleiro">
    public void draw(Graphics2D g){
        //desenha pecas Brancas
        pecasBrancas.stream().map((p) -> {
            p.draw(g);
            return p;
        }).forEach((p) -> {
        	p.generateMoves(tabuleiro, pecasBrancas, pecasPretas);
        });
        
        //desenha pecas pretas
        pecasPretas.stream().map((p) -> {
            p.draw(g);
            return p;
        }).forEach((p) -> {
            p.generateMoves(tabuleiro, pecasBrancas, pecasPretas);
        });
        
    }

    public Peca searchPeca() {
        //Percorre os grupos de peças para ver se alguma foi selecionada
        for(Peca p: pecasBrancas){
            if(p.inSquare(selectedX, selectedY)){
                return p;
            }
        }
        
        for(Peca p: pecasPretas){
            if(p.inSquare(selectedX, selectedY)){
                return p;
            }
        }
        
        return null;
    }public Peca searchPeca(int x, int y) {
        //Percorre os grupos de peças para ver se alguma foi selecionada
        for(Peca p: pecasBrancas){
            if(p.inSquare(x, y)){
                return p;
            }
        }
        
        for(Peca p: pecasPretas){
            if(p.inSquare(x, y)){
                return p;
            }
        }
        
        return null;
    }

    public void changeJogador() {
       if(jogadorAtual.getValor() == 1) jogadorAtual = Cor.BRANCO;
       else jogadorAtual = Cor.PRETO;
    }
    public static Peca sercPeca (ArrayList<Peca> w , ArrayList<Peca> b,Point p){
        for(Peca pe: w){
            if(pe.inSquare(p.x, p.y)){
                return pe;
            }
        }
        for(Peca pe: b){
            if(pe.inSquare(p.x, p.y)){
                return pe;
            }
        }
        return null;
    }

    public void tomar(Peca p) {
        selectedX = p.getQuadrante().x;
        selectedY = p.getQuadrante().y;
        if(selectedPeca.moveIsValid(selectedX, selectedY)){
            p.setVisible(false);
            System.out.println(selectedPeca+" X "+p);
            setQuadranteSelected(selectedX, selectedY);
            selectedPeca = null;
            changeJogador();
        }
    }
    
//</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Executar as Thread">
    public void runTabuleiroThread(){
        tabuleiroThread.start();
        autoSaveThread.setSleepTime(saveInterval);
        infos.run();
        try {
            infos.call();
            autoSaveThread.call();
        } catch (Exception ex) {
            Logger.getLogger(ChessModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        initGameThread.close();
        
    }
    
//</editor-fold>

    public void addSec() {
        this.counterTotal.addSec();
        if(jogadorAtual.getValor() == 1) this.counterPreto.addSec();
        else this.counterBranco.addSec();
    }
    public void addMessageToConsole(String msg){
        consoleMessage += "\n"+msg;     
    }
  


    
}   
