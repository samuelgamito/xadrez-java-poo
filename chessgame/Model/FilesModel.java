/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Model;

import chessgame.Exceptions.LoadFileException;
import java.io.*;
import java.util.Scanner;
/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class FilesModel {
    
    private static String lastFileModified(String dir) throws IOException {
        File fl = new File(dir);
        String retorno = null;
        File[] files = fl.listFiles(new FileFilter() {          
            @Override
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if (file.lastModified() > lastMod) {
                choice = file;
                lastMod = file.lastModified();
            }
        }
        if(choice != null){
           retorno = choice.getCanonicalPath();
        }
        return retorno;
    }
    
    public void saveFile(String fileName ,String put){
            //Solicitando os dados da model            
            /*
                GERENCIANDO O ARQUIVO
            */
            try {
                //Local do arquivo
                String localPath = new File(".").getCanonicalPath()
                                    +"/saves/"+fileName;
                try ( //Abrindo Arquivo
                    FileWriter file = new FileWriter(localPath)) {
                    PrintWriter printFile = new PrintWriter(file);       
                    printFile.printf("%s", put);
                    file.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        
    }
    
    @SuppressWarnings("resource")
	public void loadSave(ChessModel loadIn, String fileName) throws LoadFileException{
        try {
            String dir = new File(".").getCanonicalPath()
                                    +"/saves/";
            String localPath = dir+fileName;

            System.out.println(localPath);
            if(localPath == null)
                throw new LoadFileException("Não existe nem um jogo salvo!!!");
            try (BufferedReader br = 
                    new BufferedReader(new FileReader(localPath))){
                    

                        
            Scanner line;
            //Carregando primeira linha para a classe scanner 
            //e assim percorrer cada campo de dado do arquivo
            
            line = new Scanner(br);
            loadIn.setLoadGameSettings(line.next()
                    ,line.next()
                    ,line.next()
                    ,line.nextInt()
                    ,line.next());
            loadIn.setCounterTotal(line.nextInt(), line.nextInt());
            loadIn.setCounterBranco(line.nextInt(), line.nextInt());
            loadIn.setCounterPreto(line.nextInt(), line.nextInt());
			for(int i = 0; i<32; ++i) {//32 Pecas, corre 32 linhas
                            loadIn.setLoadPeca(line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt());
			}
                        loadIn.runTabuleiroThread();
		} catch (IOException e) {
			e.printStackTrace();
		}

        }catch(IOException e) {
            e.printStackTrace();
        }
        
    }
    public void loadSave(ChessModel loadIn) throws LoadFileException{
        try {
            String dir = new File(".").getCanonicalPath()
                                    +"/saves/";
            String localPath = lastFileModified(dir);
            if(localPath == null)
                throw new LoadFileException("Não existe nem um jogo salvo!!!");
            try (BufferedReader br = 
                    new BufferedReader(new FileReader(localPath))){
                    

                        
            Scanner line;
            //Carregando primeira linha para a classe scanner 
            //e assim percorrer cada campo de dado do arquivo
            
            line = new Scanner(br);
            loadIn.setLoadGameSettings(line.next()
                    ,line.next()
                    ,line.next()
                    ,line.nextInt()
                    ,line.next());
            loadIn.setCounterTotal(line.nextInt(), line.nextInt());
            loadIn.setCounterBranco(line.nextInt(), line.nextInt());
            loadIn.setCounterPreto(line.nextInt(), line.nextInt());
			for(int i = 0; i<32; ++i) {//32 Pecas, corre 32 linhas
                            loadIn.setLoadPeca(line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt()
                                    ,line.nextInt());
			}
                        loadIn.runTabuleiroThread();
		} catch (IOException e) {
			e.printStackTrace();
		}

        }catch(IOException e) {
            e.printStackTrace();
        }
        
    }
    
}
