/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Model;

import java.util.Formatter;
import java.util.Locale;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class TimerCounter {
    private int  m,s;
    
    public TimerCounter(){
        this.m = 00;
        this.s = 00;
    }
    public TimerCounter(int m, int s){
        this.m = m;
        this.s = s;
    }
    
    public void addSec(){
        if(s==59){
            s = 0;
            ++m;
        }else ++s;
    }
    
    @SuppressWarnings("resource")
	@Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb,Locale.US);
        return formatter.format("%02d:%02d", this.m, this.s).toString();
    }
    
    public String toSave(){
        return this.m+" "+this.s;
    }

    void setCounter(int m, int s) {
        this.m = m;
        this.s = s;
    }
}
