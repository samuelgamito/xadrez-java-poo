/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Threads;

import chessgame.Controller.GameController;
import chessgame.Model.ChessModel;
import chessgame.View.FirstMenu;
import chessgame.View.GameFrame;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class NewGame implements Runnable{
    Thread newGame;
    private final ChessModel model;
    private final GameController controller;
    private FirstMenu dataMenu;
    
    public NewGame(GameFrame view, ChessModel model, GameController controller){
        this.model      = model;
        this.controller = controller;
    }
    
    @Override
    public void run() {
        dataMenu = new FirstMenu(model, controller);
        dataMenu.setVisible(true);
    }
    
    public void start(){
        if(newGame == null){
            newGame = new Thread (this, "newGame");
            newGame.start ();
        }
    }
    public void close(){

        if(newGame != null && dataMenu !=null){
	        newGame.interrupt();
	        dataMenu.setVisible(false);
        }
    }
}
