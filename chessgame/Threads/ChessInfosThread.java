/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Threads;

import chessgame.Controller.GameController;
import chessgame.Model.ChessModel;
import chessgame.View.ChessInfos;
import java.util.concurrent.*;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class ChessInfosThread implements Callable<Object>{
    private final ChessInfos view;
    private final ChessModel model;
    private final GameController controller;

    private final ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();
    
    public ChessInfosThread(ChessInfos view, ChessModel model, GameController controller){
        this.view       = view;
        this.model      = model;
        this.controller = controller;
    }
 
    public void run() {
        controller.initChessInfos();
        view.updateTimer();
    }
   
    @Override
    public Object call() throws Exception {
        try{
            model.addSec();
            view.updateTimer();
        } finally {
            execService.schedule(this, 1, TimeUnit.SECONDS);
        }
        return null;
    }
    
}
