/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Threads;

import chessgame.Model.ChessModel;
import chessgame.Model.FilesModel;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class AutoSaveThread implements Callable<Object>{
    
    private ChessModel model;
    private int tSleep;
    private final FilesModel fileModel = new FilesModel();
    
    private final ScheduledExecutorService execService = Executors.newSingleThreadScheduledExecutor();
    
    public void setSleepTime(int tSleep){
        this.tSleep = tSleep;
    }
    public void setModel(ChessModel model){
        this.model = model;
    }

    @Override
    public Object call() throws Exception {
        try{
            
            System.out.println("AUTO-SAVE acionado");
            String put = model.getGameSettings()+"\n"
                    +model.getCounters()+"\n"
                    +model.getPecasBrancasSave()+"\n"
                    +model.getPecasPretasSave();
            String fileName = model.getFileName();
            fileModel.saveFile(fileName, put);
        } finally {
            execService.schedule(this, tSleep, TimeUnit.SECONDS);
        }
        return null;
    }
}
