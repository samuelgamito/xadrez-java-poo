/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chessgame.Threads;

import chessgame.Controller.GameController;
import chessgame.Model.ChessModel;
import chessgame.View.GameFrame;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class TabuleiroThread extends Thread{
    private final GameController controller;
    
    
    public TabuleiroThread(GameFrame view, ChessModel model, GameController controller){
        this.controller = controller;
    }
    
    @Override
    public void run() {
        controller.initTabuleiro();
    }
    
}
