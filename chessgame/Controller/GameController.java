package chessgame.Controller;

import chessgame.Exceptions.LoadFileException;
import chessgame.Exceptions.MovesException;
import chessgame.Model.ChessModel;
import chessgame.Model.FilesModel;
import chessgame.Pecas.Peca;
import chessgame.View.ChessInfos;
import chessgame.View.GameFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.SwingUtilities;


/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 * Essa é a classe que controla todos os eventos do mouse sobre o painel do 
 * tabuleiro
 */
public class GameController implements MouseListener, MouseMotionListener, ActionListener{
    GameFrame view        = null;
    ChessModel model      = null;
    FilesModel fModel     = null;
    ChessInfos gInfosView = null;
    
    //<editor-fold defaultstate="collapsed" desc="Metodos de Sobrescrita dos implements">
    @Override
    public void mouseClicked(MouseEvent e) {
    }
    public void moveByCommand(int x, int y) {
    	boolean validPosition = ( x >= 0 && x<=7 && y>=0 && y<=7);
    	System.out.println(validPosition);
    	
    	if(validPosition) {
    		 Peca temp;
             model.setSelectedCoord(x, y);

             if(model.getSelectedPeca() != null){//Verificando se ja possui alguma peça selecionada
                 if(model.searchPeca() == null){
                     if(model.getSelectedPeca().moveIsValid(x, y)){
                         System.out.println(model.getSelectedPeca()+" -> ("+x+","+y+")");
                         model.setQuadranteSelected(x, y);
                         model.setSelectedPeca(null);
                         model.changeJogador();
                        
                     }else{
                         try {
                             throw new MovesException("Movimento Invalido");
                         } catch (MovesException ex) {
                             System.err.println(ex.toString());
                             model.addMessageToConsole(ex.getMessage());
                             gInfosView.updateConsole();
                         }
                     }
                 }else{
                     temp = model.searchPeca();
                     if(temp != model.getSelectedPeca()){
                         if(temp.getCor() == model.getSelectedPeca().getCor()){
                             model.setSelectedPeca(temp);
                             System.out.println(model.getSelectedPeca());
                         }else{
                             model.tomar(temp);
                         }
                     }
                 }
             }else{
                 model.setSelectedPeca(model.searchPeca());
                 if(model.getSelectedPeca() != null){
                     if(model.getSelectedPeca().getCor() != model.getJogadorAtual()){
                         model.setSelectedPeca(null);
                         try {
                             throw new MovesException("Cor incorreta selecionada");
                         } catch (MovesException ex) {
                             System.err.println(ex.toString());
                             model.addMessageToConsole(ex.getMessage());
                             gInfosView.updateConsole();
                         }
                     }else System.out.println(model.getSelectedPeca());
                 }
             }
         }else model.setSelectedPeca(null);
             
    	 model.generateMoves();
         view.repaint();
    }
    @Override
    public void mousePressed(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)){
            int x = Math.round(e.getX()/61), y = Math.round(e.getY()/61);
            Peca temp;
            model.setSelectedCoord(x, y);

            if(model.getSelectedPeca() != null){//Verificando se ja possui alguma peça selecionada
                if(model.searchPeca() == null){
                    if(model.getSelectedPeca().moveIsValid(x, y)){
                        System.out.println(model.getSelectedPeca()+" -> ("+x+","+y+")");
                        model.setQuadranteSelected(x, y);
                        model.setSelectedPeca(null);
                        model.changeJogador();
                       
                    }else{
                        try {
                            throw new MovesException("Movimento Invalido");
                        } catch (MovesException ex) {
                            System.err.println(ex.toString());
                            model.addMessageToConsole(ex.getMessage());
                            gInfosView.updateConsole();
                        }
                    }
                }else{
                    temp = model.searchPeca();
                    if(temp != model.getSelectedPeca()){
                        if(temp.getCor() == model.getSelectedPeca().getCor()){
                            model.setSelectedPeca(temp);
                            System.out.println(model.getSelectedPeca());
                        }else{
                            model.tomar(temp);
                        }
                    }
                }
            }else{
                model.setSelectedPeca(model.searchPeca());
                if(model.getSelectedPeca() != null){
                    if(model.getSelectedPeca().getCor() != model.getJogadorAtual()){
                        model.setSelectedPeca(null);
                        try {
                            throw new MovesException("Cor incorreta selecionada");
                        } catch (MovesException ex) {
                            System.err.println(ex.toString());
                            model.addMessageToConsole(ex.getMessage());
                            gInfosView.updateConsole();
                        }
                    }else System.out.println(model.getSelectedPeca());
                }
            }
        }else model.setSelectedPeca(null);
            
        view.repaint();
    }
    
    @Override
    public void mouseReleased(MouseEvent e) {
    }
    
    @Override
    public void mouseEntered(MouseEvent e) {
    }
    
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    @Override
    public void mouseDragged(MouseEvent e) {
    }
    
    @Override
    public void mouseMoved(MouseEvent e) {
        int x =Math.round(e.getX()/61), y = Math.round(e.getY()/61);
        model.setMouseCoord(x, y);
        view.repaint();
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    
    }
//</editor-fold>
;
    public void setView(GameFrame view,ChessInfos gInfosView) {
       this.view       = view;
       this.gInfosView = gInfosView;
    }
    public void setModel(ChessModel model, FilesModel fModel) {
       this.model  = model;
       this.fModel = fModel;
    }

    public void initTabuleiro() {
        view.setVisible(true);
    }
    public void initChessInfos(){
        gInfosView.setVisible(true);
    }
    
    public boolean startNewGame(long time, String jPreto, String jBranco,
                                int tSave){
        System.out.println("Começando um novo jogo!!!");
        
        model.setGameSettings(time,jPreto, jBranco, tSave);
        model.runTabuleiroThread();
        
        
        return false;
    }
    public void loadGame(){
        try {
            //Carregando infos do arquivo
            fModel.loadSave(model);
            //iniciando tabuleiro
        } catch (LoadFileException ex) {
            System.out.println(ex.getMessage());
        }
        
    }

    public void menuSalvarClicked() {
        System.out.println("SAVE-BUTTON Acionado");
        String put = model.getGameSettings()+"\n"
                    +model.getCounters()+"\n"
                    +model.getPecasBrancasSave()+"\n"
                    +model.getPecasPretasSave();
        String fileName = model.getFileName();
        fModel.saveFile(fileName, put);
    }
}
