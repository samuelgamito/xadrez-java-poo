/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chessgame.Pecas;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class Cavalo extends Peca{

    public Cavalo(Cor cor, int x, int y)  {
        super(cor, x, y);
        pecaTipo = "CAVALO";
    }

    @Override
    public void draw(Graphics2D g) {
        int x      = super.quadrante.x*61, 
            y      = super.quadrante.y*61,
            intCor = super.cor.getValor();
            
        if(super.visible)
            g.drawImage(
                    pecasImg, 
                    x, y, x+61, y+61, 
                    3*61, intCor*61,4*61, (intCor+1)*61, 
                    null
            );
    }
    
    @Override
    public String toString() {
        if(this.cor == Peca.Cor.PRETO){
            return "Cp";
        } else {
            return "Cb";
        }
    }

    @Override
    public void generateMoves(int[][] tabuleiro,ArrayList<Peca> brancas,ArrayList<Peca> pretas) {
        int x = this.quadrante.x;
        int y = this.quadrante.y;
        int j;
        super.moves.clear();
        for(int i=2;i>=-2;--i){
            if(Math.abs(i)== 2)  j=1;
            else j=2;
            if(i!=0){
                if(x+i>=0 && x+i<=7){//Verificando se as coordenadas tao dentro
                    if(y-j>=0){
                        if(tabuleiro[y-j][x+i] == 0)
                            super.moves.add(new Point(x+i, y-j));
                        else{
                            if(super.cor == Cor.BRANCO && tabuleiro[y-j][x+i]<0){
                                super.moves.add(new Point(x+i, y-j));
                                if(Math.abs(tabuleiro[y-j][x+i]) == 1)
                                    System.out.println("Xeque");
                            }else if(super.cor == Cor.PRETO && tabuleiro[y-j][x+i]>0){
                                super.moves.add(new Point(x+i, y-j));
                                if(Math.abs(tabuleiro[y-j][x+i]) == 1)
                                    System.out.println("Xeque");
                                    
                            }
                        }
                    }
                    if(y+j<=7){
                        if(tabuleiro[y+j][x+i] == 0)
                            super.moves.add(new Point(x+i, y+j));        
                        else{
                            if(super.cor == Cor.BRANCO && tabuleiro[y+j][x+i]<0){
                                super.moves.add(new Point(x+i, y+j));
                                if(Math.abs(tabuleiro[y+j][x+i]) == 1)
                                    System.out.println("Xeque");
                            }else if(super.cor == Cor.PRETO && tabuleiro[y+j][x+i]>0){
                                super.moves.add(new Point(x+i, y+j));
                                if(Math.abs(tabuleiro[y+j][x+i]) == 1)
                                    System.out.println("Xeque");
                            }
                        }
                    }
                }
            }
        }
    }
    @Override
    public void checkMoves(ArrayList<Peca> brancas, ArrayList<Peca> pretas) {
   
    }
}
