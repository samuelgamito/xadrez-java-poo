/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chessgame.Pecas;

import chessgame.Model.ChessModel;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class Bispo  extends Peca{

    public Bispo(Cor cor, int x, int y) {
        super(cor, x, y);
        pecaTipo = "BISPO";
    }

    @Override
    public void draw(Graphics2D g) {
        int x      = super.quadrante.x*61, 
            y      = super.quadrante.y*61,
            intCor = super.cor.getValor();
            
        if(super.visible)
            g.drawImage(
                    pecasImg, 
                    x, y, x+61, y+61, 
                    2*61, intCor*61, 3*61, (intCor+1)*61, 
                    null
            );
    }
    
    @Override
    public String toString() {
        if(this.cor == Peca.Cor.PRETO){
            return "Bp";
        } else {
            return "Bb";
        }
    }


    @Override
    public void generateMoves(int[][] tabuleiro,ArrayList<Peca> brancas,ArrayList<Peca> pretas) {
        int x = this.quadrante.x;
        int y = this.quadrante.y;
        Peca p;
        super.moves.clear();
        int i=1;
        while(x+i<=7 && y+i<=7){
            
            p = ChessModel.sercPeca(brancas, pretas, new Point(x+i,y+i));
            if( p ==null)
                super.moves.add(new Point(x+i,y+i));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x+i,y+i));
                break;
            }
            ++i;
        }
        i=1;
        while(x-i>=0 && y+i<=7){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x-i,y+i));
            if( p ==null)
                super.moves.add(new Point(x-i,y+i));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x-i,y+i));
                break;
            }
            ++i;
        }
        i=1;
        while(x+i<=7 && y-i>=0){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x+i,y-i));
            if( p ==null )
                super.moves.add(new Point(x+i,y-i));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x+i,y-i));
                break;
            }
            ++i;
        }
        i=1;
        while(x-i>=0 && y-i>=0){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x-i,y-i));
            
            if( p == null)
                super.moves.add(new Point(x-i,y-i));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x-i,y-i));
                break;
            }
            ++i;
        }
            
    }

    @Override
    public void checkMoves(ArrayList<Peca> brancas, ArrayList<Peca> pretas) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}