/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chessgame.Pecas;

import chessgame.Model.ChessModel;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class Torre  extends Peca{

    public Torre(Cor cor, int x, int y) {
        super(cor, x, y);
        
        pecaTipo = "TORRE";
    }

    @Override
    public void draw(Graphics2D g) {
        int x      = super.quadrante.x*61, 
            y      = super.quadrante.y*61,
            intCor = super.cor.getValor();
        if(super.visible)
            g.drawImage(
                    pecasImg, 
                    x, y, x+61, y+61, 
                    4*61, intCor*61, 5*61, (intCor+1)*61, 
                    null
            );
    }

    @Override
    public String toString() {
        if(this.cor == Peca.Cor.PRETO){
            return "Tp";
        } else {
            return "Tb";
        }
    }

    @Override
    public void generateMoves(int[][] tabuleiro,ArrayList<Peca> brancas,ArrayList<Peca> pretas) {
        int x = this.quadrante.x;
        int y = this.quadrante.y;
        Peca p;
        super.moves.clear();
        int i=1;
        while(x+i<=7){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x+i,y));
            if( p ==null)
                super.moves.add(new Point(x+i,y));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x+i,y));
                break;
            }
            ++i;
        }
        i=1;
        while(x-i>=0){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x-i,y));
            if( p ==null)
                super.moves.add(new Point(x-i,y));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x-i,y));
                break;
            }
            ++i;
        }
        i=1;
        while(y+i<=7){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x,y+i));
            if( p ==null)
                super.moves.add(new Point(x,y+i));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x,y+i));
                break;
            }
            ++i;
        }
        i=1;
        while(y-i>=0){
            p = ChessModel.sercPeca(brancas, pretas, new Point(x,y-i));
            if( p ==null)
                super.moves.add(new Point(x,y-i));
            else{
                if(p.cor != super.cor)
                    super.moves.add(new Point(x,y-i));
                break;
            }
            ++i;
        }
    }

    @Override
    public void checkMoves(ArrayList<Peca> brancas, ArrayList<Peca> pretas) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}