/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chessgame.Pecas;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.*;
import javax.imageio.ImageIO;
/**
*
* @author Samuel de Olivera Gamito
* @version 0.9
*/
public abstract class Peca{
    
    protected final static String imgPath = "image/pecas.png";
    protected ArrayList<Point> moves;
    protected static BufferedImage pecasImg = null;    
    protected Cor cor;
    protected Point quadrante, initPosition;
    protected String pecaTipo;
    protected boolean
    firstMove = true,
    jump,//Indica se a peça pode pular as outras
    check,//Indica se essa peça esta deixando o rei em check
    visible=true;//Indica se a peça pode fazer um movimento para 
    
    /* Esse enum tem como finalidade indicar a linha em que a peça
    *  se encontra na imagem e facilitar a notação dela
    */
    public enum Cor{
        PRETO(1),BRANCO(0);
        public int valorCor;
        Cor(int valor){
            valorCor = valor;
        }
        public int getValor(){
            return valorCor;
        }
        public String getName(){
            if(this.valorCor == 1) return "Preta";
            else return "Branca";
            
        }
    }
    
    public Peca(Cor cor, int x, int y){
        moves = new ArrayList<Point>();
        this.cor          = cor;
        this.quadrante    = new Point(x,y);
        this.initPosition = new Point(x,y); 
        if(pecasImg == null){
            try {
                pecasImg = ImageIO.read(new File(imgPath));
            } catch (IOException ex) {
                Logger.getLogger(Peca.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public boolean inSquare(int x, int y){
        if(x == quadrante.x && y == quadrante.y) return true;
        else return false;
    }
    
    public void setQuadrante(int x, int y){
        quadrante.setLocation(x, y);
    }
    public Point getQuadrante(){
        return this.quadrante;
    }
    public Cor getCor(){
        return this.cor;
    }
    public ArrayList<Point> getMoves(){
        return this.moves;
    }
    
    public abstract void draw(Graphics2D g);
    
    public abstract void generateMoves(int[][] tabuleiro,ArrayList<Peca> brancas,ArrayList<Peca> pretas);
    public abstract void checkMoves(ArrayList<Peca> brancas,ArrayList<Peca> pretas);
    
    public boolean moveIsValid(int x, int y) {
        if (moves.stream().anyMatch((p) -> (p.x == x && p.y == y))) {
            return true;
        }
        return false;
    }
    public void setVisible(boolean v){
        this.visible = v;
        if(!v){
            this.quadrante.x=-1;
            this.quadrante.y=-1;
        }else{
            
            this.quadrante.x= this.initPosition.x;
            this.quadrante.y= this.initPosition.y;
            
        }
    }
    
    public void setFirstMove(boolean b){
        this.firstMove = b;           
    }
    public String getInfosSave(){
        //xi yi x y firstMove visible
        int f,v;
        if(firstMove) f = 1;
        else f = 0;
        
        if(visible) v = 1;
        else v = 0;
        return initPosition.x+" "+initPosition.y+" "+quadrante.x+" "+quadrante.y+" "+f+" "+v;
    }
}
