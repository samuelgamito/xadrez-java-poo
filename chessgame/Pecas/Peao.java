/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package chessgame.Pecas;

import chessgame.Model.ChessModel;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Samuel de Olivera Gamito
 * @version 0.9
 */
public class Peao  extends Peca{
    public Peao(Cor cor, int x, int y)  {
        super(cor, x, y);
        
        pecaTipo = "PEAO";
    }

    @Override
    public void draw(Graphics2D g) {
        
        int x      = super.quadrante.x*61, 
            y      = super.quadrante.y*61,
            intCor = super.cor.getValor();
            
        if(super.visible)
            g.drawImage(
                    pecasImg, 
                    x, y, x+61, y+61, 
                    5*61, intCor*61, 6*61, (intCor+1)*61, 
                    null
            );
    }
    
    @Override
    public String toString() {
        if(this.cor == Peca.Cor.PRETO){
            return "Pp";
        } else {
            return "Pb";
        }
    }

    @Override
    public void generateMoves(int[][] tabuleiro,ArrayList<Peca> brancas,ArrayList<Peca> pretas) {
        int yf;
        Peca temp;
        super.moves.clear();
        if(super.cor == Cor.BRANCO)
            yf = quadrante.y-1;
        else
            yf = quadrante.y+1;
        //Movimento normal
        if(ChessModel.sercPeca(pretas, brancas, new Point(this.quadrante.x,yf )) == null){
            moves.add(new Point(this.quadrante.x,yf));
        }
        
        //Movimento para comer peça
        temp = ChessModel.sercPeca(pretas, brancas, new Point(this.quadrante.x+1,yf ));
        if(temp != null && temp.cor != super.cor)
            moves.add(new Point(this.quadrante.x+1,yf ));
        temp = ChessModel.sercPeca(pretas, brancas, new Point(this.quadrante.x-1,yf ));
        if(temp != null && temp.cor != super.cor)
            moves.add(new Point(this.quadrante.x-1,yf ));
        
        //Primeiro moviemnto, pode andar dois
        if(super.firstMove){//Adicionando movimento de duas casa 
            if(super.cor == Cor.BRANCO)
                yf = quadrante.y-2;
            else
                yf = quadrante.y+2;
            if(ChessModel.sercPeca(pretas, brancas, new Point(this.quadrante.x,yf )) == null){
                moves.add(new Point(this.quadrante.x,yf));
            }
        }
           
    }

    @Override
    public void checkMoves(ArrayList<Peca> brancas, ArrayList<Peca> pretas) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}