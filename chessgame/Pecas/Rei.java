package chessgame.Pecas;

import chessgame.Model.ChessModel;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
/**
*
* @author Samuel de Olivera Gamito
* @version 0.9
*/
public class Rei extends Peca{
    public Rei(Cor cor, int x, int y) {
        super(cor, x, y);
        
        pecaTipo = "REI";
    }

    @Override
    public void draw(Graphics2D g) {
        int x      = super.quadrante.x*61, 
            y      = super.quadrante.y*61,
            intCor = super.cor.getValor();
         
        if(super.visible)   
            g.drawImage(
                    pecasImg, 
                    x, y, x+61, y+61, 
                    0*61, intCor*61, 1*61, (intCor+1)*61, 
                    null
            );
    }
    
    @Override
    public String toString() {
        if(this.cor == Peca.Cor.PRETO){
            return "Rp";
        } else {
            return "Rb";
        }
    }

    @Override
    public void generateMoves(int[][] tabuleiro,ArrayList<Peca> brancas,ArrayList<Peca> pretas) {
        int x = this.quadrante.x;
        int y = this.quadrante.y;
        this.moves.clear();
        Peca p;
        p = ChessModel.sercPeca(pretas, brancas, new Point(x+1, y));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x+1, y));
        
        p = ChessModel.sercPeca(pretas, brancas, new Point(x-1, y));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x-1, y));
        
        p = ChessModel.sercPeca(pretas, brancas, new Point(x, y+1));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x, y+1));
        
        p = ChessModel.sercPeca(pretas, brancas, new Point(x, y-1));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x, y-1));
        
        p = ChessModel.sercPeca(pretas, brancas, new Point(x+1, y+1));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x+1, y+1));
        
        p = ChessModel.sercPeca(pretas, brancas, new Point(x+1, y-1));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x+1, y-1));
        p = ChessModel.sercPeca(pretas, brancas, new Point(x-1, y+1));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x-1, y+1));
        p = ChessModel.sercPeca(pretas, brancas, new Point(x-1, y-1));
        if(p==null||p.cor != this.cor)
            this.moves.add(new Point(x-1, y-1));
            
        
        //Verificando se pode ter roque
        
            
            
    }

    @Override
    public void checkMoves(ArrayList<Peca> brancas, ArrayList<Peca> pretas) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}