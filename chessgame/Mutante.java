package chessgame;

import org.junit.Test;

import chessgame.Controller.GameController;
import chessgame.Model.ChessModel;
import chessgame.Model.FilesModel;
import chessgame.Pecas.Peao;
import chessgame.Pecas.Peca.Cor;
import chessgame.Threads.AutoSaveThread;
import chessgame.Threads.ChessInfosThread;
import chessgame.Threads.NewGame;
import chessgame.Threads.TabuleiroThread;
import chessgame.View.BoardPanel;
import chessgame.View.ChessInfos;
import chessgame.View.GameFrame;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class Mutante {
	private static final BoardPanel tabuleiro      = new BoardPanel();
	private static final GameFrame view            = new GameFrame(tabuleiro);
	private static ChessModel model          = new ChessModel();
	private static final GameController controller = new GameController();
	private static final FilesModel fModel         = new FilesModel();
	private static final ChessInfos gInfoView      = new ChessInfos(model);
	 @Test
	  public void testPeao() {
		 tabuleiro.addObserver(view);
	        
	        controller.setView(view,gInfoView);
	        controller.setModel(model, fModel);

	        view.setController(controller);
	        view.setModel(model);
	        
	        //Criando e estartando Thread do tabuleiro
	        TabuleiroThread tab     = new TabuleiroThread(view, model, controller);
	        NewGame novo            = new NewGame(view,model, controller);
	        ChessInfosThread infos  = new ChessInfosThread(gInfoView, model, controller);
	        AutoSaveThread autoSave = new AutoSaveThread();
	        autoSave.setModel(model);
	        String oldColor = model.getJogadorAtual().getName() ;//Salvando cor do jogador inicial
			controller.moveByCommand(6, 6);//selecionando um peao branco
			controller.moveByCommand(6, 5); //movendo um peao branco
			
			
			assertNotEquals(oldColor, model.getJogadorAtual().getName());
			

			controller.moveByCommand(4, 4); //movendo um peao branco
			
			assertNull(model.getSelectedPeca());
	  }
}
